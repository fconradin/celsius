import csv
import os
from datetime import datetime

from django.core.management.base import BaseCommand
from django.db import IntegrityError

from pro import models as m


class Command(BaseCommand):
    """
    Basecommand to insert data into the database from a csv file.
    To execute it, run python manage.py import_data path1 [path2, ...]
    The filename needs to be called table_name.csv, where table_name
    refers to a model name in models.py
    """
    stations = {
        '1': m.Station.objects.get(id=1),
        '2': m.Station.objects.get(id=2),
        '3': m.Station.objects.get(id=3)
    }

    def add_arguments(self, parser):
        parser.add_argument('files', nargs='+', type=str)

    @staticmethod
    def name_to_model(table_name):
        if hasattr(m, table_name):
            return getattr(m, table_name)
        else:
            raise ValueError(f'{table_name} could not be associated with a model')

    def clean_data(self, row):
        rv = {}
        for cell in row.items():
            if cell[1] == 'NA':  # clean empty entries
                continue
            if cell[0] == 'date':
                data = datetime.strptime(cell[1], '%m/%d/%Y')
            elif cell[0] == 'station':
                data = self.stations[cell[1]]
            else:
                data = cell[1]
            rv[cell[0]] = data
        return rv

    def handle(self, *args, **options):
        for f in options['files']:
            table_name = os.path.basename(f).split('.')[0]
            table = self.name_to_model(table_name)
            with open(f, newline='', encoding="utf-8-sig") as csvfile:
                reader = csv.DictReader(csvfile)
                for row in reader:
                    row = self.clean_data(row)
                    try:
                        table.objects.update_or_create(**row)
                    except IntegrityError:
                        print(f'{row} already in DB')
