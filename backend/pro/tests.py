import string
from random import choice

from django.contrib.auth import get_user_model
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken
from pro import models as m

User = get_user_model()


def random_word(length):
    letters = string.ascii_lowercase
    return "".join(choice(letters) for i in range(length))


class AuthTest(APITestCase):
    users = {}
    stations = {}

    def setUp(self):
        for i in range(1, 4):
            name = f"user_{i}"
            pwd = random_word(5)
            user = User.objects.create_user(username=name, password=pwd,)
            refresh = RefreshToken.for_user(user)
            access = refresh.access_token
            self.users[name] = {
                "obj": user,
                "pwd": pwd,
                "access": access,
                "refresh": refresh,
            }

            m.Station.objects.create(name=f"name_{i}", lon=i, lat=i)

    def test_unauthorized_requests(self):
        url = reverse("station-list")
        r = self.client.get(url)
        self.assertEquals(r.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_authorized(self):
        url = reverse("station-list")
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.users['user_1']['access']}")
        r = self.client.get(url)
        self.assertEquals(r.status_code, status.HTTP_200_OK)

    def test_stations_get(self):
        url = reverse("station-list")
        self.client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.users['user_1']['access']}")
        r = self.client.get(url)
        stations_in_db = m.Station.objects.all()
        self.assertEqual(len(r.data["results"]), len(stations_in_db))
