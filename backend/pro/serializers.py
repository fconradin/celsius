from pro import models as m
from rest_framework import serializers as s


class StationSerializer(s.ModelSerializer):

    class Meta:
        model = m.Station
        fields = ['id', 'name', 'lon', 'lat']


class WeatherSerializer(s.ModelSerializer):

    class Meta:
        model = m.Weather
        fields = ['station', 'date', 'precipitation', 'temperature']
