from rest_framework import pagination
from rest_framework import permissions as p
from rest_framework import viewsets

from pro import models as m
from pro import serializers as s


class WeatherViews(viewsets.ModelViewSet):
    """
    API endpoint that allows weather to be viewed
    """

    # permission_classes = [p.IsAuthenticated]

    queryset = m.Weather.objects.all()
    serializer_class = s.WeatherSerializer

    def retrieve(self, request, pk=None):
        pagination_class = pagination.PageNumberPagination
        paginator = pagination_class()
        queryset = m.Weather.objects.filter(station_id=pk).order_by('-date').all()
        page = paginator.paginate_queryset(queryset, request)
        serializer = s.WeatherSerializer(page, many=True)
        return paginator.get_paginated_response(serializer.data)


class StationViews(viewsets.ModelViewSet):
    """
    API endpoint that allows station info to be viewed
    """

    permission_classes = [p.IsAuthenticated]

    queryset = m.Station.objects.all()
    serializer_class = s.StationSerializer
