"""celsius URL Configuration

"""
from django.urls import include, path
from rest_framework import routers

from pro import views as v
from rest_framework_simplejwt import views as sjwt_v
from django.contrib import admin

router = routers.DefaultRouter()
router.register(r'stations', v.StationViews)
router.register(r'weather', v.WeatherViews)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(router.urls)),
    # path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    path('api/token/', sjwt_v.TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('api/refresh/', sjwt_v.TokenRefreshView.as_view(), name='token_refresh'),
]