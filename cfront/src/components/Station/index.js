import React, { Component } from 'react';
import { connect } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Redirect } from 'react-router-dom';
import { fetchWeather } from '../../redux/actions';


class Station extends Component {
    componentDidMount() {
        const station_id = parseInt(this.props.location.pathname.split("/")[2])
        this.props.dispatch(fetchWeather(station_id))
    }
        
    classes = makeStyles({
        root: {
            width: '100%',
            overflowX: 'auto',
        },
        table: {
            minWidth: 650,
        },
    });   
    
    render() {
        const station_id = parseInt(this.props.location.pathname.split("/")[2])
        const data = this.props.weather[station_id]
        console.log('station_id', station_id, 'data', data, 'state', this.props.weather);
        if (this.props.access) {
            return (
                <Paper className={this.classes.root}>
                    <Table className={this.classes.table} aria-label="simple table">
                        <TableHead>
                            <TableRow>
                                <TableCell>Date</TableCell>
                                <TableCell align="right">Precipitation (mm)</TableCell>
                                <TableCell align="right">Temperature (°C)</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.map(row => (
                                <TableRow key={row.date}>
                                    <TableCell component="th" scope="row">
                                        {row.date}
                                    </TableCell>
                                    <TableCell align="right">{row.precipitation}</TableCell>
                                    <TableCell align="right">{row.temperature}</TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
            );
        } else {
            return (<Redirect to={{ pathname: '/login', state: { from: this.props.location } }} />);
        }
    }
}


const mapStateToProps = (state) => {
    console.log('props', state.weather)
    return {
        access: localStorage.getItem('access'),
        weather: state.weather
    };
}

export default connect(mapStateToProps)(Station);