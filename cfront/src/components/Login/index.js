import React, { Component } from "react";
import "./index.css";
import { connect } from "react-redux";
import { login } from "../../redux/actions";
import Button from '@material-ui/core/Button';


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "flu",
      password: "pass",
      wrongCredentials: false
    };
  }

  componentDidMount() {
    if (this.props.access) {
      this.props.history.push("/");
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props
      .dispatch(login(this.state.username, this.state.password))
      .then(data => {
        if (data) {
            this.setState({ username: "", password: "" });
            this.props.history.push("/");
        } else this.setState({ wrongCredentials: true });
      });
  };
  handlePassword = e => {
    this.setState({ password: e.currentTarget.value });
  };
  handleUsername = e => {
    this.setState({ username: e.currentTarget.value });
  };
  render() {
    return (
      <form onSubmit={this.handleSubmit}>
        <input
          className={
            this.state.wrongCredentials ? "Login_wrongCredentials" : undefined
          }
          type="username"
          placeholder="Username"
          value={this.state.username}
          onChange={this.handleUsername}
          autoComplete="username"
        />
        <input
          className={
            this.state.wrongCredentials ? "Login_wrongCredentials" : undefined
          }
          type="password"
          placeholder="password"
          value={this.state.password}
          onChange={this.handlePassword}
          autoComplete="password"
        />
        <Button variant="contained" color="primary" type="submit">
            Submit
        </Button>
        {/* <button type="submit">Submit</button> */}
      </form>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    access: state.currentUser.access
  };
}

export default connect(mapStateToProps)(Login);
