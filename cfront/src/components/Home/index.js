import React, { Component } from 'react';
import { connect } from "react-redux";
import { makeStyles } from '@material-ui/core/styles';
import { Redirect } from 'react-router-dom';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Link from '@material-ui/core/Link';
import { fetchStations, fetchWeather } from '../../redux/actions';

class Home extends Component {

    classes = makeStyles(theme => ({
        root: {
            flexGrow: 1,
        },
        paper: {
            padding: theme.spacing(2),
            textAlign: 'center',
            color: theme.palette.text.secondary,
        },
        link: {
            margin: theme.spacing(1),
        },
    }));

    componentDidMount() {
        //TODO move this earlier in the cycle (after login click)
        this.props.dispatch(fetchStations());
    }

    changeRoutePath = (e, station_id) => {
        e.preventDefault();
        this.props.dispatch(fetchWeather(station_id));
        this.props.history.push(`/station/${station_id}/`);
    };

    render() {
        if (this.props.access) {
            return (
                <div className={this.classes.root}>
                    <Grid container spacing={3}>
                        {this.props.stations.map((station) =>
                            <Grid item xs={12} key={station.id}>
                                <Paper className={this.classes.paper}>
                                    <Link
                                        href='#'
                                        onClick={e => this.changeRoutePath(e, station.id)}
                                        className={this.classes.link}
                                        to={`/station/${station.id}/`}
                                    >
                                        {station.name}
                                    </Link>
                                </Paper>
                            </Grid>
                        )}
                    </Grid>
                </div>
            );
        } else {
            return (<Redirect to={{ pathname: '/login', state: { from: this.props.location } }} />);
        }
    }
}


const mapStateToProps = (state) => {
    return {
        access: localStorage.getItem('access'),
        stations: state.stations
    };
}

export default connect(mapStateToProps)(Home);