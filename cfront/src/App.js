import React, { Component } from "react";
import { connect } from "react-redux";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './components/Home'
import Login from './components/Login'
import Station from './components/Station'
import "./App.css";
import { verifyToken } from "./redux/actions";



class CelsiusApp extends Component {
    componentDidMount() {
        this.props.dispatch(verifyToken());
    }

    render() {
        return (
            <Router>
                <Switch>
                    <Route exact path="/" component={Home} />
                    <Route path="/station/:station_id" component={Station} />
                    <Route exact path="/login" component={Login} />
                </Switch>
            </Router>
        );
    }
}

const mapStateToProps = (state) => {
    return state
}

export default connect(mapStateToProps)(CelsiusApp);