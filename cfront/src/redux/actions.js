import { STORE_TOKENS, STORE_STATIONS, STORE_WEATHER } from "./actionTypes";
import jwtDecode from 'jwt-decode'
// import { serverUrl } from './constants';

const storeTokens = (access, refresh) => ({
    type: STORE_TOKENS,
    payload: { access, refresh }
});

const storeStations = (stations) => ({
    type: STORE_STATIONS,
    payload: stations
});

const storeWeather = (station_id, weather) => ({
    type: STORE_WEATHER,
    payload: { station_id, weather }
});

export const fetchStations = () => (dispatch, getState) => {
    const state = getState();
    const token = localStorage.getItem('access')

    const headers = new Headers({
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
    });
    const config = { headers, method: "GET" };

    fetch(`http://localhost:8000/stations`, config)
        .then(res => {
            if (res.status === 200) {
                //TODO error handling
                return res.json();
            }
        })
        .then(stations => {
            if (stations && stations.results) {
                dispatch(storeStations(stations.results));
            }
        });
};

export const fetchWeather = (station_id) => (dispatch, getState) => {
    const state = getState();
    const token = localStorage.getItem('access')

    const headers = new Headers({
        "Content-Type": "application/json",
        "Authorization": `Bearer ${token}`
    });
    const config = { headers, method: "GET" };
    const url = `http://localhost:8000/weather/${station_id}`
    fetch(url, config)
        .then(res => {
            if (res.status === 200) {
                //TODO error handling
                return res.json();
            }
        })
        .then(weather => {
            console.log("weather", weather)
            if (weather && weather.results) {
                dispatch(storeWeather(station_id, weather.results));
            }
        });
};

export const verifyToken = () => (dispatch, getState) => {
    let access = localStorage.getItem("access");
    let refresh = localStorage.getItem("refresh");

    if (access) {
        const current_time = new Date().getTime() / 1000;
        const accessDecoded = jwtDecode(access);
        if (current_time > accessDecoded.exp) {
            localStorage.clear();
            // TODO implement refresh token mechanism
        } else {
            dispatch(storeTokens(access, refresh));
        }
    }
}


export const login = (username, password) => (dispatch, getState) => {
    const headers = new Headers({ "Content-Type": "application/json" });
    const body = JSON.stringify({ username, password });
    const config = { headers, method: "POST", body };
    return fetch(`http://localhost:8000/api/token/`, config)
        .then(res => {
            if (res.status === 200) {
                //TODO error handling
                return res.json();
            }
        })
        .then(credentials => {
            console.log("credentials", credentials)
            if (credentials) {
                // const access = JSON.stringify(credentials.access);
                // const refresh = JSON.stringify(credentials.refresh);
                localStorage.setItem("access", credentials.access);
                localStorage.setItem("refresh", credentials.refresh);
                dispatch(storeTokens(credentials.access, credentials.refresh));
                return true;
            }
        });
};
