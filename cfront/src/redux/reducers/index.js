import { combineReducers } from "redux";
import { STORE_TOKENS, STORE_STATIONS, STORE_WEATHER } from "../actionTypes";



const currentUser = (state = {}, action) => {
    switch (action.type) {
        case STORE_TOKENS:
            return { access: action.payload.access, refresh: action.payload.refresh }
        default:
            return state;
    }
}

const stations = (state = [], action) => {
    switch (action.type) {
        case STORE_STATIONS:
            return action.payload
        default:
            return state;
    }
}

const weather = (state = { 1: [], 2: [], 3: [] }, action) => {
    switch (action.type) {

        case STORE_WEATHER:
            let station_id = parseInt(action.payload.station_id);
            let new_dict = { ...state }
            new_dict[station_id] = action.payload.weather
            return new_dict
        default:
            return state
    }
}

export default combineReducers({ currentUser, stations, weather });

