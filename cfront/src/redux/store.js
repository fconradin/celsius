import { createStore, applyMiddleware } from "redux";
import rootReducer from "./reducers";
import thunk from 'redux-thunk';


const initialState = {
    currentUser: { 'access': localStorage.getItem('access'), 'refresh': localStorage.getItem('refresh') },
    stations: [],
    weather: { 1: [], 2: [], 3: [] }

};
export default createStore(rootReducer, initialState, applyMiddleware(thunk));