FROM python:3.7

RUN wget -qO- https://deb.nodesource.com/setup_12.x | bash
RUN apt-get update && apt-get upgrade -y && apt-get install -qqy nodejs

# INSTALL BACKEND DEPENDENCIES
COPY ./backend/requirements.txt /app/requirements.txt
RUN pip install -r /app/requirements.txt

# INSTALL FRONTEND DEPENDENCIES
WORKDIR /cfront
COPY ./cfront/package.json /cfront/
COPY ./cfront/package-lock.json /cfront/
RUN npm install

# BUILD FRONTEND DEPENDENCIES
COPY ./cfront /cfront
RUN npm run build

WORKDIR /backend
COPY ./backend /backend
RUN python manage.py collectstatic --noinput

# COPY NGINX CONF
COPY ./nginx /nginx

WORKDIR /backend